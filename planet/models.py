from django.db import models


class Planet(models.Model):
    title = models.CharField(max_length=100, unique=True, verbose_name="Название планеты")

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Список планет'
        verbose_name_plural = "Список планет"

