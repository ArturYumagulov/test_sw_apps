from django.apps import AppConfig


class PlanetConfig(AppConfig):
    name = 'planet'
    verbose_name = "Планеты"
