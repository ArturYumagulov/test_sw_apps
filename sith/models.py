from django.db import models
from planet.models import Planet


class SithModel(models.Model):
    name = models.CharField(max_length=100, verbose_name="Имя")
    planet = models.ForeignKey(Planet, on_delete=models.PROTECT, verbose_name="Планета на которой обучает")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Ситх"
        verbose_name_plural = "Ситхи"