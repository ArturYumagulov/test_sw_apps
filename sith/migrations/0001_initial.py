# Generated by Django 3.0.6 on 2020-05-19 14:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('planet', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='SithModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Имя')),
                ('planet', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='planet.Planet', verbose_name='Планета на которой обучает')),
            ],
            options={
                'verbose_name': 'Ситх',
                'verbose_name_plural': 'Ситхи',
            },
        ),
    ]
