from django.apps import AppConfig


class RecrutConfig(AppConfig):
    name = 'recrut'
    verbose_name = "Список рекрутов"
