from django.urls import path
from .views import RecrutCreateView


urlpatterns = [
    path('create', RecrutCreateView.as_view(), name='create')
]