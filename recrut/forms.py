from django import forms
from .models import Recrut
from planet.models import Planet


class RecrutForm(forms.ModelForm):
    name = forms.CharField(label="Имя", widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'Введите Ваше имя'}))

    planet = forms.ModelChoiceField(label="Планета", queryset=Planet.objects.all(), widget=forms.Select(
        attrs={'class': 'form-control', 'placeholder': 'Введите название планеты'}))

    age = forms.IntegerField(label="Возраст", widget=forms.NumberInput(
        attrs={'class': 'form-control', 'placeholder': 'Время  в пути'}))

    email = forms.EmailField(label="email",
                             widget=forms.EmailInput(attrs={'class': 'form-control',
                                                            'placeholder': 'Введите электронный адрес'}))

    class Meta:
        model = Recrut
        fields = ('name', 'planet', 'age', 'email')
