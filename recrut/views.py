from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView
from .forms import RecrutForm


class RecrutCreateView(CreateView):
    template_name = 'recrut/create.html'
    form_class = RecrutForm
    success_url = reverse_lazy('home')

