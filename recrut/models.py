from django.db import models
from planet.models import Planet


class Recrut(models.Model):
    name = models.CharField(max_length=100, verbose_name="Имя")
    planet = models.ForeignKey(Planet, on_delete=models.PROTECT, verbose_name="Планета")
    age = models.PositiveSmallIntegerField(verbose_name="Возраст")
    email = models.EmailField(verbose_name="Адрес электронной почты", unique=True)

    def __str__(self):
        return f"Рекрут {self.name} с планеты {self.planet}"

    class Meta:
        verbose_name = "Рекрут"
        verbose_name_plural = "Рекруты"