from django.db import models


class Questions(models.Model):
    text = models.TextField(verbose_name="Вопросы")

    def __str__(self):
        return self.text


class Answer(models.Model):
    question = models.ForeignKey(Questions, on_delete=models.PROTECT)
    result = models.BooleanField()
