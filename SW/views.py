from django.shortcuts import render


def home(request):
    context = {'title_name': 'Главная', 'name': 'Гость'}
    return render(request, 'home.html', context)
